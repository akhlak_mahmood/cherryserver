import cherrypy

# just a simple logging module
import cherryserver._serv as serv
import cherryserver.tools.log as log
from cherryserver.tools.log import cf
from cherryserver.tools.utils import launchAsChromeApp

from cherryserver.root import Root

def start(rootDir, serveOn = ("127.0.0.1", 8080), launchView=True):
    # we are placing these here so that they don't
    # pollute the cherryserver namespace
    import os
    try:
        import urllib2 as _urllib
    except ImportError:
        import urllib.request as _urllib

    serverconfig = \
        {
            '/': {
                'tools.sessions.on': True,
                'tools.encode.on': True,
                'tools.encode.encoding': 'utf-8',
                'tools.gzip.on': True
            },
            '/favicon.png': {
                'tools.staticfile.on': True,
                'tools.staticfile.filename': os.path.join(rootDir, "cherryserver/media/favicon.png")
            },
            '/media': {
                'tools.staticdir.on': True,
                'tools.staticdir.dir': os.path.join(rootDir, "cherryserver/media/")
            }
        }


    serv.Root = rootDir
    serv.IP   = serveOn[0]
    serv.Port = serveOn[1]
    serv.Config = serverconfig

    cherrypy.server.socket_host = serveOn[0]
    cherrypy.server.socket_port = serveOn[1]
    cherrypy.server.thread_pool = 10
    cherrypy.server.environment = "production"
    cherrypy.request.show_traceback = False


    if launchView:
        launchAsChromeApp(serv.home_url(), rootDir)
        log.say(cf(), "Launched Google Chrome in app mode at %s" %serv.home_url())

    try:
        # try to connect to the given ip and port.
        _urllib.urlopen("http://%s:%d/" % serveOn, timeout=3)

    except _urllib.URLError:
        # we are okay to go.
        root = Root()
        log.say(cf(), "Starting CherryServer on http://{}:{}", serveOn[0], serveOn[1])
        cherrypy.quickstart(root, config=serverconfig)

    else:
        # either the ip port is not available, or a previous version
        # of cherry server is already serving on it.
        log.warn(cf(), "A server already running on http://{}:{}", serveOn[0], serveOn[1])
        input()
