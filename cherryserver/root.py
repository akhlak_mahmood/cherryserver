import cherrypy

# just a simple logging module
import cherryserver._serv as serv
import cherryserver.tools.log as log
from cherryserver.tools.log import cf

from cherryserver.tools import html

from cherryserver.ajax import Ajax
from cherryserver.spfjs import SPFJSDemo

class Root:
    def __init__(self):
        self.ajax = Ajax()
        self.spfjs = SPFJSDemo()
        log.say(cf(), "Route '/' (root) initialized.")

    @cherrypy.expose
    def index(self, **kwargs):

        # response = "Hello world\n{}".format(kwargs)
        response = html.template("homepage", title="CherryServer Home")

        return "{}".format(response)

    @cherrypy.expose
    def default(self, *args, **kwargs):
        response = html.template("404", title="Error 404, Not found")
        return "{}".format(response)



