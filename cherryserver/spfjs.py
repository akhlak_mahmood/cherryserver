import cherrypy
import json

# just a simple logging module
import cherryserver._serv as serv
import cherryserver.tools.log as log
from cherryserver.tools.log import cf

from cherryserver.tools import html

class SPFJSDemo:
    def __init__(self):
        log.say(cf(), "SPFJSDemo initialized.")

    @cherrypy.expose
    def index(self, **kwargs):
        resp = html.template("spfjsdemo", title="SPFJS Demo")
        return "{}".format(resp)

    @cherrypy.expose
    def respond(self, **kwargs):
        if "spf" in kwargs:
            # we are responding to spfjs request

            resp = {
                "title": "The Response",
                "head": "<link name=\"toastr\" href=\"/media/style/toastr.css\" rel=\"stylesheet\">",
                "body": {
                    "content": "Hello world",
                },
                "foot": "<script name=\"toastrjs\" src=\"/media/script/toastr.js\"></script>"\
                        "<script>toastr[\"success\"](\"Page successfully loaded using SPFJS!\");</script>"
            }

            # response should be json for spfjs
            cherrypy.response.headers['Content-Type'] = "application/json"
            
            # convert our response dictionary to json,
            # the returning json must be utf-8 encoded
            return json.dumps(resp).encode()

        else:
            # we are not dealing with spfjs
            resp = html.template("spfjsdemo", title="SPFJS Demo")
            return "{}".format(resp)
