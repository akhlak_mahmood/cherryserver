$(document).ready(function() {

    nav_open = false;
    $('body').addClass('nav-collapsed');

    $('.offcanvas').mouseenter(function() {
        //alert("offcanvas mouseentered");
        $('body').removeClass('nav-collapsed');
        nav_open = true;
    });

    $('.offcanvas').mouseleave(function() {
        //alert("offcanvas mouseleft");
        $('body').addClass('nav-collapsed');
        nav_open = false;
    });

    $('.content-frame').click(function() {
        $('body').addClass('nav-collapsed');
        nav_open = false;
    });

    // windows get resized whenever the address bar 
    // visibility is toggled
    // $( window ).resize( function() {
    //     alert("window resized");
    //     if ($("body").width() < 640) {
    //         $('body').addClass('nav-collapsed');
    //         nav_open = false;
    //     }
    //     else {
    //         //$('body').removeClass('nav-collapsed');
    //     }
    // });

    // Menu Toggle
    $('.toggle-me').on('click', function(e) {
        e.preventDefault();
        $('body').toggleClass('nav-collapsed');
    });

    $(window).scroll(function () {
        document.body.scrollLeft = 0;
        
        if ($(this).scrollTop() > 125) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
