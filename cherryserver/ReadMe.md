Design rules:

    All the route handlers should be in the cherryserver/

    The _serv.py contains the superglobals, and the only non route file in cherryserver/

    The common server helpers will be in the cherryserver/tools/

    The static media files (images, js, css) inside cherryserver/media/

    Any static files (html, xml, txt) inside cherryserver/static/

    Any view or template files in the cherryserver/template/
    Any media files related to a single view should also be placed inside cherryserver/media
    However, it should be in a seperate directory named after the view it corresponds to.

    Any data handler or model files should be inside cherryserver/model/

    Any raw data files in the cherryserver/data/



Also:

    Module names must be lower_cased, underscore allowed.
        Route handler should named after the http path.
        The route handler class should be CapitalCased.

    Package names must be lower cased, one word.

    Class, Interface, Exception names must be CapitalCased.

    Methods names must be lower_cased with underscore.

    Global variables should be CapitalCased.

    Local variables, parameters should be camelCased or all lowercase.

    Function names must be lower_cased with underscore.

    Private variables and methods should start with an underscore.
