"""
    All the globals and superglobals should be referenced here.
    Anything on the server that needs to be updated from different
    places should be stored in a variable in here.

    Variable names should be capital cased.
"""

# default variables

Root        = None
IP          = None
Port        = None
Config      = {}





# util getter functions
def home_url():
    if IP == 80:
        return "http://{}/".format(IP)        
    else:
        return "http://{}:{}/".format(IP, Port)
