from __future__ import print_function, unicode_literals
from os.path import basename as bn
import inspect

__doc__ = """ A simple logging module.

    Just import the module and use the 3 functions.

        import cherryserver.log as log

        log.set_log_tag("LOGGER")
        log.set_log_debug(True)

        log.say(log.cf(), "Hello, this is an info message.")
        log.warn(log.cf(), "Say() messages will not be logged if debug is not True.")
        log.error(log.cf(), "Error() and Warn() will always be logged.")

    String formating can be done easily.

        from cherryserver.log import cf

        name = "Jack Sparrow"
        age = 23
        location = "Earth"

        log.warn(cf(), "2 + 2 = {}, 3 * 2 = {}", 2+2, 3*2)
        log.say(cf(), "Hello, I am {0}, {1}, speaking from {2}.", name, age, location)

"""

# When debug is True, say() messages will not be logged.
# And the warning messages will be logged as normal messages.
Debug = False

# If a file name is given, messages will be logged there.
LogFile = False

# The log tag
Tag = "Log"

def _flog(message, header=None):
    """ Helper function to log to a file. """
    if header is None:
        header = "[{}]".format(Tag.upper())
    if LogFile:
        try:
            fh = open(LogFile, 'a+')
            fh.write(header + " " + message + "\n")
            fh.close()
        except: pass


def say(frame, message, *formatargs):
    """ Log a message. Frame info must be passed using log.cf(). """

    header = "{}::MSG".format(Tag.upper())

    if type(message) != str:
        message = "{}".format(message)
    else:
        message = message.format(*formatargs)

    if Debug:
        print("{} {} [{}(), {}, {}]".format(header,
            message, frame.function, bn(frame.filename), frame.lineno))

    _flog("{} in {}() at {} line {}".format(message,
        frame.function, bn(frame.filename), frame.lineno), header)

    return message



def warn(frame, message, *formatargs):
    """ Log a warning message. Frame info must be passed using Log.cf(). """

    if Debug:
        header = "{}::WARNING".format(Tag.upper())
    else:
        header = "{}::MSG".format(Tag.upper())
    
    if type(message) != str:
        message = "{}".format(message)
    else:
        message = message.format(*formatargs)

    print("{} {} [{}(), {}, {}]".format(header,
        message, frame.function, bn(frame.filename), frame.lineno))

    _flog("{} in {}() at {} line {}".format(message,
        frame.function, bn(frame.filename), frame.lineno), header)

    return message


def error(frame, message, *formatargs):
    """ Log an error message. Frame info must be passed using Log.cf(). """

    header = "{}::ERROR".format(Tag.upper())

    if type(message) != str:
        message = "{}".format(message)
    else:
        message = message.format(*formatargs)

    print("{} {} [{}(), {}, {}]".format(header,
        message, frame.function, bn(frame.filename), frame.lineno))

    _flog("{} in {}() at {} line {}".format(message,
        frame.function, bn(frame.filename), frame.lineno), header)

    return message

def cf():
    """ Alias of current_frame(). """
    return inspect.getframeinfo(inspect.currentframe().f_back)


def set_log_debug(debug):
    """ Set the current debug. """
    global Debug
    Debug = debug

def set_log_tag(tag):
    """ Set the log tag. """
    global Tag
    Tag = tag

def ON():
    """ Turn debug on. """
    global Debug
    Debug = True
    

def OFF():
    """ Turn debug off. """
    global Debug
    Debug = False


def xsay(frame, message, *formatargs):
    """ A helper function to skip say() easily. """
    return message


if __name__ == "__main__":
    print(__doc__)
    input()
