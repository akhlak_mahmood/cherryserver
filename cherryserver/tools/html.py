import os
from cherryserver.tools import pyratemp

import cherryserver._serv as serv
import cherryserver.tools.log as log
from cherryserver.tools.log import cf

def template(tmplname, data={}, **kwargs):

    if tmplname.endswith(".tmpl"):
        tmplname += ".html"
    elif not tmplname.endswith(".tmpl.html"):
        tmplname += ".tmpl.html"

    tmplname = os.path.join(serv.Root, "cherryserver/template/", tmplname)

    if not os.path.isfile(tmplname):
        raise FileNotFoundError(tmplname)

    try:
        t = pyratemp.Template(filename=tmplname)
    except Exception as err:
        log.error(cf(), err)
        raise err

    # populate with some common data
    data.update({
        "serv": serv
        })

    data.update(kwargs)

    return t(**data)
