import os
import sys

# FREEZING REQUIREMENTS
if getattr(sys, 'frozen', False):
    sys.path.append(os.path.join(os.path.dirname(sys.executable), 'bin'))
    sys.path.append(os.path.join(os.path.dirname(sys.executable), 'bin\\library.zip'))
    absDir = os.path.join(os.getcwd(), os.path.dirname(sys.executable))
else:
    absDir = os.getcwd()
#FREEZING REQUIREMENTS



# just a simple logging module
import cherryserver
cherryserver.log.set_log_tag("########")
cherryserver.log.set_log_debug(True)



if __name__ == '__main__':

    cherryserver.log.say(cherryserver.log.cf(), "Hello there!")
    cherryserver.log.say(cherryserver.log.cf(), "Starting HTTP server. Please wait.")

    try:
        import cherrypy
    except (ImportError, NameError):
        input("""Error: Can not find the module 'cherrypy'.

        PyVGDemo depends on CherryPy.
        Please install CherryPy first. You can install it using pip.
        Execute "pip install cherrypy" in your command prompt.
        Or visit http://www.cherrypy.org for other options.
        """)
    else:
        cherryserver.start(absDir)

    cherryserver.log.say(cherryserver.log.cf(), "Goodbye.")
